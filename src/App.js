import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "./App.css";
import MainPanel from "./MainPanel";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" component={MainPanel} />
          <Route path="/profile" component={MainPanel} />
          <Route path="/documents" component={MainPanel} />
          <Route path="/dashboard" component={MainPanel} />
          <Route path="/leaderboard" component={MainPanel} />
          <Redirect to="/profile" />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
