import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles, Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const classes = this.props.classes;
    return (
      <Paper className={classes.paper}>
        <Grid xs={12} container>
          <Grid xs={3} style={{ marginTop: 10, color: "white" }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
          <Grid xs={6} style={{ color: "white" }}>
            RANK BREAKDOWN
          </Grid>
          <Grid xs={3} style={{ marginTop: 10, color: "white", height: 5 }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
        </Grid>
        <Grid xs={12} container>
          <Grid
            xs={12}
            container
            style={{ padding: 10, justifyContent: "space-between" }}
          >
            <Grid xs={5}>
              <Typography style={{ fontSize: 10, color: "white" }}>
                Score Breakdown
              </Typography>
              <Paper
                style={{
                  backgroundColor: "#39014A",
                  color: "white",
                  padding: 10
                }}
              >
                <Grid xs={12} container>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    3400
                  </Grid>
                  <Grid xs={2}>
                    <div
                      style={{
                        borderLeft: "3px solid #F4511E",
                        height: "30px",
                        marginLeft: "10px",
                        marginTop: "5px"
                      }}
                    ></div>
                  </Grid>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    7000
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            <Grid xs={5}>
              <Typography style={{ fontSize: 10, color: "white" }}>
                Time Breakdown
              </Typography>

              <Paper
                style={{
                  backgroundColor: "#39014A",
                  color: "white",
                  padding: 10
                }}
              >
                <Grid xs={12} container>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    3400
                  </Grid>
                  <Grid xs={2}>
                    <div
                      style={{
                        borderLeft: "3px solid #0043B5",
                        height: "30px",
                        marginLeft: "10px",
                        marginTop: "5px"
                      }}
                    ></div>
                  </Grid>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    7000
                  </Grid>
                </Grid>{" "}
              </Paper>
            </Grid>
            <Grid xs={5}>
              <Typography style={{ fontSize: 10, color: "white" }}>
                Attempts Breakdown
              </Typography>
              <Paper
                style={{
                  backgroundColor: "#39014A",
                  color: "white",
                  padding: 10
                }}
              >
                <Grid xs={12} container>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    3400
                  </Grid>
                  <Grid xs={2}>
                    <div
                      style={{
                        borderLeft: "3px solid #FFDD00",
                        height: "30px",
                        marginLeft: "10px",
                        marginTop: "5px"
                      }}
                    ></div>
                  </Grid>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    7000
                  </Grid>
                </Grid>{" "}
              </Paper>
            </Grid>
            <Grid xs={5}>
              <Typography style={{ fontSize: 10, color: "white" }}>
                Accuracy Breakdown
              </Typography>
              <Paper
                style={{
                  backgroundColor: "#39014A",
                  color: "white",
                  padding: 10
                }}
              >
                <Grid xs={12} container>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    3400
                  </Grid>
                  <Grid xs={2}>
                    <div
                      style={{
                        borderLeft: "3px solid #43A047",
                        height: "30px",
                        marginLeft: "10px",
                        marginTop: "5px"
                      }}
                    ></div>
                  </Grid>
                  <Grid xs={5} style={{ fontSize: 25, fontWeight: "bold" }}>
                    7000
                  </Grid>
                </Grid>{" "}
              </Paper>
            </Grid>
          </Grid>
        </Grid>
        <Grid xs={12} container>
          <Grid xs={4} style={{ marginTop: 10 }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
          <Grid xs={4} style={{ color: "white" }}>
            ACHIEVEMENTS
          </Grid>

          <Grid xs={4} style={{ marginTop: 10 }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
        </Grid>

        <Grid xs={12} container>
          <Grid xs={3}>
            <img
              src={require("../Icons/insightful_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={3}>
            <img
              src={require("../Icons/quick answer_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={3}>
            <img
              src={require("../Icons/easy repeat_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={3}>
            <img
              src={require("../Icons/time saver_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
        </Grid>
        <Grid xs={12} container style={{ padding: 10 }}>
          <Grid xs={3}>
            <img
              src={require("../Icons/insightful_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={9} container>
            <Grid xs={3} style={{ fontSize: 15, color: "white" }}>
              Insightful
            </Grid>
            <Grid
              xs={12}
              style={{ fontSize: 10, textAlign: "left", color: "white" }}
            >
              Able to answer questions right off the bat and there is the value
              of the bat
            </Grid>
          </Grid>

          <Grid xs={3}>
            <img
              src={require("../Icons/quick answer_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={9} container>
            <Grid xs={3} style={{ fontSize: 15, color: "white" }}>
              Insightful
            </Grid>
            <Grid
              xs={12}
              style={{ fontSize: 10, textAlign: "left", color: "white" }}
            >
              Able to answer questions right off the bat and there is the value
              of the bat
            </Grid>
          </Grid>

          <Grid xs={3}>
            <img
              src={require("../Icons/easy repeat_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={9} container>
            <Grid xs={3} style={{ fontSize: 15, color: "white" }}>
              Insightful
            </Grid>
            <Grid
              xs={12}
              style={{ fontSize: 10, textAlign: "left", color: "white" }}
            >
              Able to answer questions right off the bat and there is the value
              of the bat
            </Grid>
          </Grid>

          <Grid xs={3}>
            <img
              src={require("../Icons/time saver_icon.svg")}
              color="white"
              width="50"
              height="60"
              alt="ff"
            />
          </Grid>
          <Grid xs={9} container>
            <Grid xs={3} style={{ fontSize: 15, color: "white" }}>
              Insightful
            </Grid>
            <Grid
              xs={12}
              style={{ fontSize: 10, textAlign: "left", color: "white" }}
            >
              Able to answer questions right off the bat and there is the value
              of the bat
            </Grid>
          </Grid>
        </Grid>
        <Grid xs={12} container>
          <Grid xs={3} style={{ marginTop: 10 }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
          <Grid xs={6} style={{ color: "white" }}>
            TRAINING SESSIONS
          </Grid>
          <Grid xs={3} style={{ marginTop: 10 }}>
            <Divider style={{ backgroundColor: "white" }} />
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

const styles = () => ({
  paper: {
    minHeight: "90vh",
    backgroundColor: "#080037"
  }
});

export default withStyles(styles)(Main);
