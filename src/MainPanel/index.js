import React, { Component } from "react";
import {
  Grid,
  AppBar,
  Toolbar,
  Typography,
  BottomNavigation,
  BottomNavigationAction,
  withStyles
} from "@material-ui/core";
import Main from "./../Main";
import { Link } from "react-router-dom";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const classes = this.props.classes;
    return (
      <div style={{ fontFamily: "Open Sans, Montserrat" }}>
        <AppBar
          position="fixed"
          style={{
            backgroundColor: "#39014A",
            padding: 5,
            borderRadius: 5,
            boxShadow: "none",
            border: "solid",
            borderColor: "#39014A",
            position: "sticky"
          }}
        >
          <Toolbar>
            <img
              src={require("../Icons/mastero_logo.svg")}
              width="75"
              height="75"
              alt="ff"
            />
            <Grid container xs={12}>
              <Grid xs={9}>
                <Typography className={classes.typo} variant="h5">
                  Ajay Bahadur
                </Typography>
              </Grid>
              <Grid xs={9} container style={{ marginLeft: "10px" }}>
                <Grid xs={3}>3400</Grid>
                <Grid xs={3}>55%</Grid>
                <Grid xs={3}>12</Grid>
                <Grid xs={3}>7</Grid>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <Main />
        <BottomNavigation className={classes.stick} showLabels>
          <BottomNavigationAction
            component={Link}
            to="/dashboard"
            label="DashBoard"
            icon={
              <img
                src={require("../Icons/dashboard_bottom_nav_icon.svg")}
                color="white"
                width="40"
                height="40"
                alt="ff"
              />
            }
          />
          <BottomNavigationAction
            component={Link}
            to="/leaderboard"
            label="LeaderBoard"
            icon={
              <img
                src={require("../Icons/leaderboard_bottom_nav_icon.svg")}
                width="40"
                height="40"
                alt="ff"
              />
            }
          />
          <BottomNavigationAction
            component={Link}
            to="/documents"
            label="Documents"
            icon={
              <img
                src={require("../Icons/documents_bottom_nav_icon.svg")}
                width="40"
                height="40"
                alt="ff"
              />
            }
          />
          <BottomNavigationAction
            component={Link}
            to="/profile"
            label="Profile"
            icon={
              <img
                src={require("../Icons/profile_bottom_nav_icon.svg")}
                width="40"
                height="40"
                alt="ff"
              />
            }
          />
        </BottomNavigation>
      </div>
    );
  }
}

const styles = () => ({
  stick: {
    position: "sticky",
    bottom: 0,
    backgroundColor: "#39014A",
    paddingBottom: 10
  },
  typo: {
    fontFamily: "Montserrat",
    fontStyle: "bold"
  }
});

export default withStyles(styles)(NavBar);
